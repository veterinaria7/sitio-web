
// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-app.js";
import { getDatabase, ref, set, child, get, update, remove, onValue } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-database.js"
import { getStorage, ref as refImage, uploadBytes, getDownloadURL  } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-storage.js"
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBE-69vUfN8KAlmU2UTzvQC9QJu_BR6iYA",
  authDomain: "proyectofinal-cc104.firebaseapp.com",
  databaseURL: "https://proyectofinal-cc104-default-rtdb.firebaseio.com",
  projectId: "proyectofinal-cc104",
  storageBucket: "proyectofinal-cc104.appspot.com",
  messagingSenderId: "898455303544",
  appId: "1:898455303544:web:c79f93d0a64bd07cd7848d",
  storageBucket: "gs://proyectofinal-cc104.appspot.com"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();
const storage = getStorage(app);

//declaracion de objetos

var servicio;
var precio;
var descripcion;
var estatus;
var seccion;
var fileName;
var urlFileName;
var urlpath = "";

const serviciosList = { 1: "Servicios Médicos", 2: "Servicios Peluquería", 3: "Accesorios" }

//Botones

var btnAgregar = document.getElementById('btnAgregar');
var btnMostrar = document.getElementById('btnMostrar');
var btnActualizar = document.getElementById('btnActualizar');
var btnModificar = document.getElementById('btnModificar');
var btnTodos = document.getElementById("btnTodos");
var btnBorrar = document.getElementById('btnBorrar');

function insertarDatos() {
  leerInputs();
  if (servicio != "" && precio != "" && descripcion != "") {
    let fileUpload = document.getElementById("files").files[0];

    /* Image */
    fileName = new Date().getTime().toString();
    const storageRef = refImage(storage, fileName);
    uploadBytes(storageRef, fileUpload).then((snapshot) => {
      getDownloadURL(storageRef).then((url) => {
        set(ref(db, `servicios/${serviciosList[seccion]}/` + servicio), {
          servicio: servicio, precio: precio, descripcion: descripcion, status: estatus, seccion: seccion, urlFileName: url
        }).then((resp) => {
          alert("Se realizó el registro");
          document.getElementById("files").value = null;
        }).catch((error) => {
          alert("Surgio un Error" + error);
        })
      });
    });
  }
  else {
    alert("NO DEJE CAMPOS VACIOS");
  }


}

function actualizar() {
  leerInputs();
  let fileUpload = document.getElementById("files").files[0];
  if(fileUpload === undefined){
    update(ref(db, `servicios/${serviciosList[seccion]}/` + servicio), {
      servicio: servicio,
      precio: precio,
      descripcion: descripcion,
      status: estatus,
      seccion: seccion,
      urlFileName: urlpath
    })
    .then((resp) => {
      alert("Se ha actualizado con éxito");
      mostrarProductos();
    })
    .catch((error) => {
      alert("Ocurrio un error: " + error);
    })
  }else{
    fileName = new Date().getTime().toString();
    const storageRef = refImage(storage, fileName);
    uploadBytes(storageRef, fileUpload).then((snapshot) => {
      getDownloadURL(storageRef).then((url) => {
        update(ref(db, `servicios/${serviciosList[seccion]}/` + servicio), {
          servicio: servicio,
          precio: precio,
          descripcion: descripcion,
          status: estatus,
          seccion: seccion,
          urlFileName: url
        })
        .then((resp) => {
          document.getElementById("imagenMuestra").src = url;
          document.getElementById("files").value = null;
          alert("Se ha actualizado con éxito");
          mostrarProductos();
        })
        .catch((error) => {
          alert("Ocurrio un error: " + error);
        })
      });
    });
    
  }
}

function mostrarDatos() {
  leerInputs();
  console.log("mostrar datos ");
  const dbref = ref(db);
  get(child(dbref, `servicios/${serviciosList[seccion]}/` + servicio))
    .then((snapshot) => {
      if (snapshot.exists()) {
        servicio = snapshot.val().servicio;
        precio = snapshot.val().precio;
        descripcion = snapshot.val().descripcion;
        console.log(descripcion);
        seccion = snapshot.val().seccion;
        urlpath = snapshot.val().urlFileName;
        document.getElementById("imagenMuestra").src = urlpath; 
        llenarInputs();
      }
      else {
        alert("No existe");
      }
    }).catch((error) => {
      alert("error buscar" + error);
    });
}

function llenarInputs() {
  document.getElementById('servicio').value = servicio;
  document.getElementById('precio').value = precio;
  document.getElementById('descripcion').value = descripcion;
  document.getElementById('seccion').value = seccion;
  document.getElementById('status').value = estatus;
  document.getElementById('urlpath').value = urlpath;
}

function borrar() {
  leerInputs();
  remove(ref(db, `servicios/${serviciosList[seccion]}/` + servicio))
    .then(() => {
      alert("se borro");
    })
    .catch((error) => {
      alert("causo Error " + error);
    });

}

function leerInputs() {
  servicio = document.getElementById('servicio').value;
  precio = document.getElementById('precio').value;
  descripcion = document.getElementById('descripcion').value;
  seccion = document.getElementById('seccion').value;
  estatus = document.getElementById('status').value;

}

function mostrarProductos() {
  var lista = document.getElementById('lista');
  var htmlTemplate = "";
  for (let index = 1; index <= Object.keys(serviciosList).length; index++) {
    var dbRef = ref(db, `servicios/${serviciosList[index]}/`);
    onValue(dbRef, (snapshot) => {
        snapshot.forEach((childSnapshot) => {
          const childKey = childSnapshot.key;
          const childData = childSnapshot.val();
          htmlTemplate += `
            <div class="campo"> 
              ${childKey} 
              ${childData.servicio} 
              ${childData.precio} 
              ${childData.descripcion} 
              ${serviciosList[childData.seccion]} 
              <br> 
            </div>`;
        });
        lista.innerHTML = htmlTemplate;
    }, {
      onlyOnce: true
    });
  }
}


btnAgregar.addEventListener('click', insertarDatos);
btnMostrar.addEventListener('click', mostrarDatos);
btnBorrar.addEventListener('click', borrar);
btnModificar.addEventListener('click', actualizar);
btnTodos.addEventListener('click', mostrarProductos);