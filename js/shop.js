// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-app.js";
import { getDatabase, ref, set, child, get, update, remove, onValue } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-database.js"
import { getStorage, ref as refImage, uploadBytes, getDownloadURL  } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-storage.js"
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBE-69vUfN8KAlmU2UTzvQC9QJu_BR6iYA",
  authDomain: "proyectofinal-cc104.firebaseapp.com",
  databaseURL: "https://proyectofinal-cc104-default-rtdb.firebaseio.com",
  projectId: "proyectofinal-cc104",
  storageBucket: "proyectofinal-cc104.appspot.com",
  messagingSenderId: "898455303544",
  appId: "1:898455303544:web:c79f93d0a64bd07cd7848d",
  storageBucket: "gs://proyectofinal-cc104.appspot.com"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();
const storage = getStorage(app);

document.getElementById('operaciones').onclick = function (){
    let dbRef = ref(db, 'servicios/Servicios Médicos/');
    onValue(dbRef, (snapshot) =>{
        var containerShop = document.getElementById('containerShop');
        var productsHTML = "";
        containerShop.innerHTML = productsHTML;
        snapshot.forEach((childSnapshot) => {
            let childKey = childSnapshot.key;
            let childData = childSnapshot.val();
            productsHTML +=`
            <div class="product">
                <div class="headerProduct">
                    <img src="${childData.urlFileName}">
                    <h3>${childData.servicio}</h3>
                </div>
                <div class="bodyProduct">
                    <p>${childData.descripcion}</p>
                </div>
                <div class="footerProduct">
                    <h2>${childData.precio}</h2>
                </div>
            </div>`;
        }) 
        containerShop.innerHTML = productsHTML;
    })
}

document.getElementById('peluqueria').onclick = function (){
    let dbRef = ref(db, 'servicios/Servicios Peluquería/');
    onValue(dbRef, (snapshot) =>{
        var containerShop = document.getElementById('containerShop');
        var productsHTML = "";
        containerShop.innerHTML = productsHTML;
        snapshot.forEach((childSnapshot) => {
            let childKey = childSnapshot.key;
            let childData = childSnapshot.val();
            productsHTML +=`
            <div class="product">
                <div class="headerProduct">
                    <img class="product" src="${childData.urlFileName}">
                    <br>
                    <h3 >${childData.servicio}</h3>

                </div>
                <div class="bodyProduct">
                    <p>${childData.descripcion}</p>
                </div>
                <div class="footerProduct">
                <br>
                    <h2>${childData.precio}</h2>
                </div>
            </div>`;
        }) 
        containerShop.innerHTML = productsHTML;
    })
}

document.getElementById('accesorio').onclick = function (){
    let dbRef = ref(db, 'servicios/Accesorios/');
    onValue(dbRef, (snapshot) =>{
        var containerShop = document.getElementById('containerShop');
        var productsHTML = "";
        containerShop.innerHTML = productsHTML;
        snapshot.forEach((childSnapshot) => {
            let childKey = childSnapshot.key;
            let childData = childSnapshot.val();
            productsHTML +=`
            <div class="product">
                <div class="headerProduct">
                    <img src="${childData.urlFileName}">
                    <h3>${childData.servicio}</h3>
                </div>
                <div class="bodyProduct">
                    <p>${childData.descripcion}</p>
                </div>
                <div class="footerProduct">
                    <h2>${childData.precio}</h2>
                </div>
            </div>`;
        }) 
        containerShop.innerHTML = productsHTML;
    })
}