// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-app.js";
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-auth.js";
import { getDatabase, ref, set, child, get, update, remove, onValue } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-database.js"
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBE-69vUfN8KAlmU2UTzvQC9QJu_BR6iYA",
  authDomain: "proyectofinal-cc104.firebaseapp.com",
  databaseURL: "https://proyectofinal-cc104-default-rtdb.firebaseio.com",
  projectId: "proyectofinal-cc104",
  storageBucket: "proyectofinal-cc104.appspot.com",
  messagingSenderId: "898455303544",
  appId: "1:898455303544:web:c79f93d0a64bd07cd7848d",
  storageBucket: "gs://proyectofinal-cc104.appspot.com"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();
const auth = getAuth();

document.getElementById('signin').onclick = function(){
    let email = document.getElementById('email').value;
    let password = document.getElementById('password').value;
    
    if( email != "" && password != ""){
        createUserWithEmailAndPassword(auth, email, password)
            .then((userCredential) => {
                // Signed in
                var user = userCredential.user;
                set(ref(db, 'users/' + user.uid), {
                    email: email,
                    password: password
                }).then(()=>{
                    alert('creado en real time')
                }).catch((error) => {
                    alert(error);
                })
            })
            .catch((error) => {
                console.log(error)
            });
    }else{
        alert('RELLENE TODOS LOS CAMPOS')
    }
}
document.getElementById('login').onclick = function(){
    let email = document.getElementById('email').value;
    let password = document.getElementById('password').value;
    debugger
    if (email != "" && password != "") {
        signInWithEmailAndPassword(auth, email, password)
            .then((usercredential) => {
                location.href = "/html/admin.html";
            })
            .catch((error) => {
                switch (error.code) {
                    case 'auth/invalid-email':
                        alert('Usuario o contraseña incorrecto')
                        return;

                    default:
                        alert('Algo salio mal.')
                        return;
                }
            })
    } else {
        alert('RELLENE TODOS LOS CAMPOS');
    }
}
